window = typeof window === 'undefined' ? {} : window;
document = typeof document === 'undefined' ? window.document || {} : document;
window.cordova = typeof window.cordova === 'undefined' ? {} : window.cordova;

window.cordova.file = {
	// Read-only directory where the application is installed.
	applicationDirectory: location.origin + '/',
	// Where to put app-specific data files.
	dataDirectory: 'filesystem:file:///persistent/',
	// Cached files that should survive app restarts.
	// Apps should not rely on the OS to delete files in here.
	cacheDirectory: 'filesystem:file:///temporary/'
};

